<?php
/**
 * Class Replacer
 *
 * @package Replacer
 * @since 1.0.1
 */

if ( ! class_exists( 'Replacer' ) ) :

	class Replacer {

		/**
		 * The single instance of this plugin.
		 *
		 * @see    Replacer()
		 *
		 * @access private
		 * @var    Replacer
		 */
		private static $instance;

		/** @var array The plugin settings array. */
		public $settings = array();

		/** @var array The plugin data array. */
		public $data = array();

		/**
		 * Constructor. Doesn't actually do anything as instance() creates the class instance.
		 */
		private function __construct() {}

		/**
		 * Creates a new instance of this class if one hasn't already been made
		 * and then returns the single instance of this class.
		 *
		 * @return Replacer
		 */
		public static function instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new Replacer;
				self::$instance->init();
			}

			return self::$instance;
		}

		/**
		 * Register all of the needed hooks and actions.
		 */
		protected function init() {
			// Add actions.
			add_action( 'admin_menu', [ $this, 'add_admin_menu_page' ] );
			add_action( 'admin_init', [ $this, 'register_settings' ] );
			add_action( 'wp_enqueue_scripts', [ $this, 'replacer_front_enqueue' ] );

			// Add filters.
			add_filter( 'the_content', [ $this, 'replacer_front' ] );
		}

		/**
		 * Adds Settings page.
		 */
		public function add_admin_menu_page() {
			add_menu_page(
				__( 'Replacer', 'replacer' ),
				__( 'Replacer', 'replacer' ),
				'manage_options',
				'replacer',
				[ $this, 'admin_menu_page' ],
				'dashicons-randomize',
				66
			);
		}

		/**
         * Settings page template.
         *
         * @see add_admin_menu_page
		 */
		public function admin_menu_page() {
			?>
            <div class="wrap">
                <h2><?php echo get_admin_page_title(); ?></h2>
            </div>

			<?php //if( get_current_screen()->parent_base !== 'options-general' ) settings_errors('replacer_fields');
			?>

            <form action="options.php" method="POST">
				<?php settings_fields( 'replacer_fields' ); ?>
				<?php do_settings_sections( 'replacer' ); ?>
				<?php submit_button( __( 'Save', 'replacer' ) ); ?>
            </form>
			<?php
		}

		public function register_settings() {
			$this->register_settings_group(
				__( 'Fields', 'replacer' ),
				'replacer_fields',
				[
					__( 'Input', 'replacer' )  => 'input',
					__( 'Output', 'replacer' ) => 'output'
				]
			);
		}

		/**
		 * @param string $title
		 * @param $option_group
		 * @param $fields
		 */
		public function register_settings_group( $title, $option_group, $fields ) {
			$option_group_section = $option_group . '_section';
			add_settings_section(
				$option_group_section,
				$title,
				'',
				'replacer'
			);

			foreach ( $fields as $key => $field ) {
				$option_field = $option_group . "_" . $field;
				add_settings_field(
					$option_field,
					__( 'Input', 'replacer' ),
					[ $this, $option_field . "_callback" ],
					'replacer',
					$option_group_section
				);

				register_setting( $option_group, $option_field );
				$this->settings[ $option_group ][ $key ] = $option_field;
			}
		}

		/**
		 * Callback function
		 *
		 * @see replacer_fields
		 * @since 1.0.1
		 */
		public function replacer_fields_input_callback() {
			$replacer_fields_input = '<label><input name="replacer_fields_input" type="text" value="' . get_option( 'replacer_fields_input' ) . '"></label>';
			echo $replacer_fields_input;
		}

		/**
		 * Callback function
		 *
		 * @see replacer_fields
		 * @since 1.0.1
		 */
		public function replacer_fields_output_callback() {
			$replacer_fields_output = '<label><input name="replacer_fields_output" type="text" value="' . get_option( 'replacer_fields_output' ) . '"></label>';
			echo $replacer_fields_output;
		}

		/**
		 * Replaces all inputs to a outputs in the content.
		 *
		 * @param $content
		 * @return mixed|string
		 */
		public function replacer_front( $content ) {

			if ( ! empty( $content ) && $this->check_post_type() ) {
				$inputs  = get_option( 'replacer_fields_input' );
				$outputs = get_option( 'replacer_fields_output' );

				$this->data['replacements'] = $this->get_replacements( $content, $inputs, $outputs );

				if ( empty($this->data['replacements']) ) {
					return $content;
				}

				$content = $this->do_replacements( $content );

				// Sends result object with replacements to front.
				wp_localize_script( 'replacer-front-js', 'replacer_front_data', [
					'replacements' => $this->data['replacements']
				] );
			}

			return $content;
		}

		/**
		 * @return bool
		 */
		private function check_post_type() {
			$post_types_whitelist = [ 'post' ];
			$post_type            = get_post_type();

			return in_array( $post_type, $post_types_whitelist );
		}

		/**
		 * @param $content
		 * @param $inputs
		 * @param $outputs
		 *
		 * @return array|bool
		 */
		private function get_replacements( $content, $inputs, $outputs ) {

			if ( empty( $inputs ) || empty( $outputs ) ) {
				return false;
			}

			$replacer_fields_delimiter = ',';
			// Admin fields converted to array by delimiter.
			$inputs  = explode( $replacer_fields_delimiter, $inputs );
			$outputs = explode( $replacer_fields_delimiter, $outputs );

			// Content without tags and shortcodes.
			$clean_content = sanitize_text_field( $content );
			$clean_content = strip_shortcodes( $clean_content );

			// Converts content to words array.
			$words        = explode( ' ', $clean_content );
			$word_pattern = implode( '|', $inputs );

			$replacements = [];
			foreach ( $words as $word ) {
				// Searches inputs inside word.
				if ( preg_match( "/$word_pattern/", $word ) ) {
					// Replaces inputs to outputs in word.
					foreach ( $inputs as $input ) {
						// Replacing original word, if modified before.
						$word = ( isset( $replacements['modified'][ $word ] ) ) ? $replacements['modified'][ $word ] : $word;

						// Save word as original.
						$replacements['original'][ $word ] = $word;

						// Parses the word into parts.
						$word_parts         = explode( $input, $word );
						$replacements_count = count( $word_parts ) - 1;

						// Save word as modified with filter before modified word.
						/* @param string $modified_word_before */
						$replacements['modified'][ $word ] = apply_filters( 'replacer_modified_word_before', $modified_word_before = '' );

						// Shuffles replacement parts, for random word in result.
						shuffle( $outputs );
						// Assembles a word from parts, replacing occurrences.
						foreach ( $word_parts as $current_replacement => $word_part ) {
							$replacements['modified'][ $word ] .= $word_parts[ $current_replacement ];
							$replacements['modified'][ $word ] .= ( $current_replacement != $replacements_count ) ? $outputs[ $current_replacement ] : '';
						}

						// Save word as modified with filter after modified word.
						/* @param string $modified_word_after */
						$replacements['modified'][ $word ] .= apply_filters( 'replacer_modified_word_after', $modified_word_after = '' );
					}
				}
			}

			return ( !empty($replacements) ) ? $replacements : false;
		}

		/**
		 * @param $content
		 *
		 * @return string
		 */
		private function do_replacements( $content ) {

			/* @param string $content_wrapper_before */
			$content_wrapper_before = apply_filters( 'replacer_content_wrapper_before', $content_wrapper_before = '<div class="replacer_content_wrapper">' );

			// replaces all modified in content.
			$modified_content = str_replace( $this->data['replacements']['original'], $this->data['replacements']['modified'], $content );

			/* @param string $content_wrapper_after */
			$content_wrapper_after = apply_filters( 'replacer_content_wrapper_after', $content_wrapper_after = '</div>' );

			return $content_wrapper_before . $modified_content . $content_wrapper_after;

		}

		public function replacer_front_enqueue() {
			wp_enqueue_script( 'replacer-front-js', REPLACER_PLUGIN_URL . '/public/js/front.js', [ 'jquery' ], false, true );
		}

		public function plugin_activation() {

		}

		public function plugin_deactivation() {

		}

		public static function plugin_uninstall() {
			delete_option( 'replacer_fields_input' );
			delete_option( 'replacer_fields_output' );
		}

		/**
		 * Prevents the class from being cloned.
		 */
		public function __clone() {
			wp_die( "Please don't clone Replacer" );
		}

		/**
		 * Prints the class from being unserialized and woken up.
		 */
		public function __wakeup() {
			wp_die( "Please don't unserialize/wakeup Replacer" );
		}

	}

endif; // class_exists check