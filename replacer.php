<?php
/**
 * Plugin Name: Replacer
 * Plugin URI: https://bitbucket.org/Yurasik/replacer/src/master/
 * Description: Find $input and replace to $output for post_type="post" at the_content()
 * Version: 1.0.1
 * Author: Popov Yuriy
 * Author URI: https://iwebdealer.com/
 * License: GPL v2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: replacer
 *
 * @package Replacer
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

define( 'REPLACER_VERSION', '1.0.1' );
define( 'REPLACER_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'REPLACER_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once( REPLACER_PLUGIN_DIR . 'class.replacer.php' );
$Replacer = Replacer();

/**
 * Initialize this plugin once all other plugins have finished loading.
 */
add_action( 'init', 'Replacer' );

/**
 * Returns the single instance of this plugin, creating one if needed.
 *
 * @return Replacer
 */
function Replacer() {
	return Replacer::instance();
}

register_activation_hook( __FILE__, [ $Replacer, 'plugin_activation' ] );
register_deactivation_hook( __FILE__, [ $Replacer, 'plugin_deactivation' ] );
register_uninstall_hook( __FILE__, [ 'Replacer', 'plugin_uninstall' ] );