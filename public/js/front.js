(function($) {

  // Checks for an object with replacements
  if (typeof replacer_front_data === 'undefined') {
    return;
  }

  // defines variables
  let $modified = replacer_front_data.replacements.modified;
  let $replacer_content_wrapper = $('.replacer_content_wrapper');
  let $content = $replacer_content_wrapper.html();

  front();
  function front() {
    // Finds all modified words in content and replaces them
    $.each($modified, function(original, modified) {
      $content = $content.replace(new RegExp(modified, 'g'),
          '<span class=\'modified_word\'>' + modified + '</span>');
    });

    // Refreshes the content on the Front Page
    $content = $replacer_content_wrapper.html($content);

    // Finds all modified words and sets background color to yellow
    $content.find('.modified_word').each(function() {
      $(this).css('background-color', 'yellow');
    });
  }

  //front_with_tag_filter();
  function front_with_tag_filter() {
    let $tags_filter_list = ['<img', '<video', '<audio'];
    let $parsedHTML = $.parseHTML($content);

    $($parsedHTML).each(function() {
      let $this = this;
      let $html = $(this).html();

      if ($.trim($html).length) {
        $.each($modified, function(original, modified) {
          let $tags_filter = true;
          $.each($tags_filter_list, function(key, value) {
            if ($html.search(new RegExp(value, 'g')) !== -1) {
              $tags_filter = false;
            }
          });

          if ($tags_filter) {
            $html = $html.replace(new RegExp(modified),
                '<span class=\'modified_word\'>' + modified + '</span>');
            $($this).html($html);
          }
        });
      }
    });
    var $collectedHTML = $($parsedHTML).html($($parsedHTML).clone()).html();

    $content = $replacer_content_wrapper.html($collectedHTML);
    $content.find('.modified_word').each(function() {
      $(this).css('background-color', 'yellow');
    });
  }

})(jQuery);

